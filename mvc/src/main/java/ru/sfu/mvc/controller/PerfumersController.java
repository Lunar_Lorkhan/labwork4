package ru.sfu.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sfu.mvc.model.Perfumery;
import ru.sfu.mvc.repos.PerfumeryRepository;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class PerfumersController {
    private final PerfumeryRepository perfumeryRepository;

    @Autowired
    public PerfumersController(PerfumeryRepository perfumeryRepository) {
        this.perfumeryRepository = perfumeryRepository;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        model.addAttribute("title", "Все записи");
        model.addAttribute(perfumeryRepository.findAll().stream().sorted(
                Comparator.comparing(Perfumery::getId)).collect(Collectors.toList())
        );
        return "layouts/start";
    }

    @GetMapping("/new")
    public String addPerfumers(Model model) {
        model.addAttribute("title", "Добавить");
        model.addAttribute("perfumery", new Perfumery());
        return "layouts/new";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("title", "Редактировать");
        Optional<Perfumery> perfumeryOptional = perfumeryRepository.findById(id);
        if (perfumeryOptional.isPresent()) {
            model.addAttribute(perfumeryOptional.get());
            return "layouts/edit";
        } else {
            return "layouts/start";
        }
    }

    @PostMapping("/createPerfumery")
    public String createPerfumery(@ModelAttribute("perfumery") Perfumery perfumery) {
        System.out.println(perfumery);
        perfumeryRepository.save(perfumery);
        return "redirect:/";
    }

    @PatchMapping("/patchPerfumery")
    public String editPerfumery(@ModelAttribute("perfumery") Perfumery perfumery) {
        System.out.println(perfumery);
        perfumeryRepository.save(perfumery);
        return "redirect:/";
    }

    @DeleteMapping("/deletePerfumery")
    public String deletePerfumery(@ModelAttribute("perfumery") Perfumery perfumery) {
        perfumeryRepository.deleteById(perfumery.getId());
        return "redirect:/";
    }
}
